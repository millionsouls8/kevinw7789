// funcs
async function loadToC() {
    try {
        const response = await fetch('../src/include/topbar.html');
        if (!response.ok) {
            throw new Error('Could not fetch topbar.html');
        }
        const tocHtml = await response.text();
        document.getElementById('topbar').innerHTML = tocHtml;
    } catch (error) {
        console.error('Failed to load:', error);
    }
}

// listners
document.addEventListener('DOMContentLoaded', () => {
    loadToC();
});

// page functions

window.toggleImages = function(button) {
    var targetId = button.getAttribute("data-target");
    var imageContainer = document.getElementById(targetId);

    if (imageContainer.style.display === "none") {
        imageContainer.style.display = "grid";
    } else {
        imageContainer.style.display = "none";
    }
}